﻿using Microsoft.AspNetCore.Mvc;

namespace CoreVueKendo.MVC.Controllers
{
    public class ManufacturerController : Controller
    {
        public IActionResult Grid()
        {
            return View();
        }

        public IActionResult Form()
        {
            return View();
        }
    }
}
