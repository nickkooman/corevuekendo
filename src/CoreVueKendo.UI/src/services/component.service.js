import Vue from 'vue';

export default {
  mountComponents(imports) {
    /*
      Expected Input:
        [Array of Objects || Object of Objects || Object]
        imports = [{

          [Vue Component]
          component: Component,

          [DOM Element name string]
          element: String

        }]

      Expected Output:
        Vue components are mounted to each corresponding custom element that is rendered on the DOM.
    */

    let components = imports;

    // Check if input is a single object.
    if (!(Array.isArray(imports) && typeof imports === 'object')) {
      components = [imports];
    }

    components.forEach(({ component, element }) => {
      // Is the custom element in the DOM?
      if (!document.querySelector(element)) {
        return;
      }

      // Create a new Vue instance and mount it to the custom element.
      new Vue({
        render(createElement) {
          return createElement(component);
        }
      }).$mount(element);
    });
  }
};
