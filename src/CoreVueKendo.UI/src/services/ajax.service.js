import Constants from '../constants/constants';

export default {
  async getData(url) {
    let data;

    // Create mock data.
    if (url === Constants.api.manufacturer.items) {
      const productNames = [
        'Chai',
        'Chang',
        'Syrup',
        'Apple',
        'Orange',
        'Banana',
        'Lemon',
        'Pineapple',
        'Tea',
        'Milk'
      ];
      const unitPrices = [12.5, 10.1, 5.3, 7, 22.53, 16.22, 20, 50, 100, 120];

      data = Array(50)
        .fill({})
        .map((_, index) => ({
          ProductID: index + 1,
          ProductName: productNames[Math.floor(Math.random() * productNames.length)],
          UnitPrice: unitPrices[Math.floor(Math.random() * unitPrices.length)]
        }));
    }

    if (url === Constants.api.manufacturer.columns) {
      data = [
        { field: 'ProductID' },
        { field: 'ProductName', title: 'Product Name' },
        { field: 'UnitPrice', title: 'Unit Price' }
      ];
    }

    if (url === Constants.api.unit.items) {
      const productNames = [
        'Tenderloin',
        'Porterhouse',
        'New York Strip',
        'Wagyu',
        'Short Rib',
        'Chuck'
      ];
      const unitPrices = [22.53, 16.22, 20, 50, 100, 120];

      data = Array(50)
        .fill({})
        .map((_, index) => ({
          ProductID: index + 1,
          ProductName: productNames[Math.floor(Math.random() * productNames.length)],
          UnitPrice: unitPrices[Math.floor(Math.random() * unitPrices.length)]
        }));
    }

    if (url === Constants.api.unit.columns) {
      data = [
        { field: 'ProductID' },
        { field: 'ProductName', title: 'Product Name' },
        { field: 'UnitPrice', title: 'Unit Price' }
      ];
    }

    return data;
  }
};
