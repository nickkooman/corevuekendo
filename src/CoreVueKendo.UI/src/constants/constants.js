export default {
  api: {
    manufacturer: {
      items: 'items-manufacturer',
      columns: 'columns-manufacturer'
    },
    unit: {
      items: 'items-unit',
      columns: 'columns-unit'
    }
  }
};
