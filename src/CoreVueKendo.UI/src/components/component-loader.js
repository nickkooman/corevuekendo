// Services
import ComponentService from '../services/component.service';

// Components
import ManufacturerGrid from './manufacturer/ManufacturerGrid.vue';
import ManufacturerForm from './manufacturer/ManufacturerForm.vue';
import UnitGrid from './unit/UnitGrid.vue';
import UnitForm from './unit/UnitForm.vue';

/*
  All component elements should be the kebab-cased version of the component name.
  ex. ExampleComponent => example-component
*/
const imports = [
  {
    component: ManufacturerGrid,
    element: 'manufacturer-grid'
  },
  {
    component: ManufacturerForm,
    element: 'manufacturer-form'
  },
  {
    component: UnitGrid,
    element: 'unit-grid'
  },
  {
    component: UnitForm,
    element: 'unit-form'
  }
];

export default {
  loadComponents() {
    ComponentService.mountComponents(imports);
  }
};
