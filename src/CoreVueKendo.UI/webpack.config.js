﻿const path = require('path');
const webpack = require('webpack');

// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CleanPlugin = require('clean-webpack-plugin');
const ExtractCssPlugin = require('mini-css-extract-plugin');
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin');
const PostBuildDeletePlugin = require('webpack-delete-after-emit');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const PRODUCTION_ENVIRONMENT = 'production';
const DEVELOPMENT_ENVIRONMENT = 'development';

const getConfig = env => {
  const combinedEnvironment = { ...process.env, ...env };

  const appEnvironment =
    combinedEnvironment.NODE_ENV === PRODUCTION_ENVIRONMENT
      ? PRODUCTION_ENVIRONMENT
      : DEVELOPMENT_ENVIRONMENT;

  const sourceDir = path.resolve(__dirname, './src');
  const styleDir = path.resolve(sourceDir, './styles');
  const buildDir = path.resolve('../CoreVueKendo.MVC/wwwroot/dist');

  const getSourcePath = srcPath => path.resolve(sourceDir, srcPath);

  const babelLoader = {
    loader: 'babel-loader',
    options: {
      presets: [['@babel/preset-env', { modules: false, useBuiltIns: 'usage', corejs: 3 }]],
      plugins: [
        '@babel/plugin-proposal-object-rest-spread',
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        '@babel/plugin-proposal-class-properties'
      ]
    }
  };

  const eslintLoader = {
    loader: 'eslint-loader',
    options: {
      failOnError: appEnvironment === PRODUCTION_ENVIRONMENT
    }
  };

  return {
    devtool: 'source-map',
    entry: {
      master: getSourcePath('index.js'),
      // 'sample-module': getSourcePath('modules/sample-module.js'),
      style: `${styleDir}/style.scss`
    },
    mode: appEnvironment,
    module: {
      rules: [
        {
          test: /\.(jpeg|jpg|png|svg|woff|woff2|eot|ttf)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                // encodes files < 32kB as base64 data urls
                limit: 32000,
                name: 'assets/[hash].[ext]'
              }
            }
          ]
        },

        {
          test: /\.scss$/,
          use: ['vue-style-loader', 'css-loader', 'sass-loader']
        },

        {
          exclude: /(node_modules|bower_components)/,
          include: sourceDir,
          test: /\.js?/,
          use: [{ ...babelLoader }]
        },

        {
          enforce: 'pre',
          exclude: /(node_modules|bower_components)/,
          include: sourceDir,
          test: /\.js?/,
          use: [{ ...eslintLoader }]
        },

        {
          test: /\.css$/,
          use: [ExtractCssPlugin.loader, 'css-loader']
        },

        {
          test: /\.vue$/,
          loader: 'vue-loader'
        }
      ]
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendors: {
            chunks: 'all',
            name: 'vendor',
            test: /[\\/]node_modules[\\/]/
          }
        }
      },
      // These plugins are only run in production mode
      minimizer: [
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true,
          terserOptions: {
            compress: {
              comparisons: true,
              conditionals: true,
              dead_code: true,
              drop_console: true,
              if_return: true,
              join_vars: true,
              unused: true,
              warnings: false
            },
            output: {
              comments: false
            }
          }
        }),
        new OptimizeCssPlugin({
          cssProcessorOptions: {
            map: { inline: false }
          }
        })
      ]
    },
    output: {
      filename: '[name].min.js',
      chunkFilename: '[name].min.js',
      globalObject: 'this',
      path: `${buildDir}`,
      publicPath: '/wwwroot/dist/'
    },
    plugins: [
      // new BundleAnalyzerPlugin(),
      new webpack.ProgressPlugin(),

      new webpack.IgnorePlugin(/^(mv|rim-raf|source-map-support)$/),

      // deletes BUILD_DIR before rebuilds
      new CleanPlugin({
        cleanOnceBeforeBuildPatterns: [`${buildDir}/**`],
        dangerouslyAllowCleanPatternsOutsideProject: true
      }),

      // defines global variables
      new webpack.DefinePlugin({
        DEBUG: appEnvironment === PRODUCTION_ENVIRONMENT
      }),

      // extract css from bundles, and compile them into a single css file
      new ExtractCssPlugin({
        filename: '[name].min.css'
      }),

      // delete the bundle webpack generates for the SASS entry point
      // new PostBuildDeletePlugin({
      //   globs: ['style.min.js*']
      // }),

      // run stylelint for linting scss files on build
      new StyleLintPlugin({
        configFile: '.stylelintrc',
        failOnError: appEnvironment === PRODUCTION_ENVIRONMENT,
        ignorePath: '.stylelintignore'
      }),

      // Clone any other rules you have defined and apply them to the corresponding language blocks in .vue files.
      // For example, if you have a rule matching /\.js$/, it will be applied to <script> blocks in .vue files.
      new VueLoaderPlugin()
    ],
    resolve: {
      // make importing common modules cleaning
      alias: {
        images: `${sourceDir}/images`
        /*
        constants: `${sourceDir}/constants.js`,
        lib: `${sourceDir}/lib`,
        modules: `${sourceDir}/modules`
        */
      },
      extensions: ['*', '.js', '.jsx', '.json', '.scss']
    },
    target: 'web'
  };
};

module.exports = getConfig;
